﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Infrastructure.CookieStorage
{
    
    public enum CookieDataKeys
    {
        BasketItems,
        BasketTotal,
        BasketId
    }
}
