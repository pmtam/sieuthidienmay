﻿using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService
{
    public class CreateBasketResponse
    {
        public BasketView Basket { get; set; }
    }
}
