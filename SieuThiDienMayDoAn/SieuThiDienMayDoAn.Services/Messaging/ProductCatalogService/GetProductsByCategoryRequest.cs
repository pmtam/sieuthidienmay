﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService
{
    public class GetProductsByCategoryRequest
    {
        public GetProductsByCategoryRequest()
        {
           
        }
        public int CategoryId { get; set; }
        
        public ProductsSortBy SortBy { get; set; }
        public int Index { get; set; }
        public int NumberOfResultsPerPage { get; set; }
    }
}
