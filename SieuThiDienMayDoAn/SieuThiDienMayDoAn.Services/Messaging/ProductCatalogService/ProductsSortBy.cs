﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService
{
    public enum ProductsSortBy
    {
        PriceHighToLow = 1,
        PriceLowToHigh = 2
    }
}
