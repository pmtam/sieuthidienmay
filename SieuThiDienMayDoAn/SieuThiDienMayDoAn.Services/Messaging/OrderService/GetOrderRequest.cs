﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.OrderService
{
    public class GetOrderRequest
    {
        public int OrderId { get; set; }
    }
}
