﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.OrderService
{
    public class CreateOrderRequest
    {
        public int DeliveryAddressId { get; set; }
        public string BasketId { get; set; }
        public string UserName { get; set; }
        public int DeliveryOptionId { get; set; }
       
    }
}
