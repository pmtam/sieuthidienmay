﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.BasketService
{
    public class UpdateBasketItemsResponse
    {
        public string BasketTotal { get; set; }
        public int NumberOfItems { get; set; }
    }
}
