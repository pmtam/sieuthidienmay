﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.ReviewRatingService
{
    public class GetAllReviewByProductRequest
    {
        public int ProductId { get; set; }
    }
}
