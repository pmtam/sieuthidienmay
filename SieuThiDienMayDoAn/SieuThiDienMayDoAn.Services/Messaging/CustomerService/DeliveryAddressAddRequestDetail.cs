﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.CustomerService
{
    public class DeliveryAddressAddRequestDetail
    {
        public string UserName { get; set; }
        public string NumberOfHouse { get; set; }
        public string Street { get; set; }
        public string Ward { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
    }
}
