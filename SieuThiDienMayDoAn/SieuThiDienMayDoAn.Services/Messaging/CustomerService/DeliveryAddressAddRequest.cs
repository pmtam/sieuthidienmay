﻿using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.CustomerService
{
    public class DeliveryAddressAddRequest
    {
        public string UserName { get; set; }
        public DeliveryAddressView Address { get; set; }
    }
}
