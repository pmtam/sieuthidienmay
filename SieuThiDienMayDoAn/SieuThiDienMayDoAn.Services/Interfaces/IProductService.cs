﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Services.Messaging.ProductService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Interfaces
{
    public interface IProductService : IService<Product, int>
    {
        GetFeaturedProductsResponse GetFeaturedProducts();
        GetProductResponse GetProduct(GetProductRequest request);
        GetBestSellerProductsResponse GetBestSellerProducts();
        GetSpecialProductsResponse GetSpecialProducts();
        GetLatestProductsResponse GetLatestProducts();
        IEnumerable<ProductSummaryView> SearchProducts(string searchStr, int index, int numberOfRecordsPerPage);
    }
}
