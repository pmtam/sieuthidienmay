﻿using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Interfaces
{
    public interface IProductCatalogService
    {
        
        GetProductsByCategoryResponse GetProductsByCategory(GetProductsByCategoryRequest request);
        IEnumerable<ProductSummaryView> GetProductsBySuperCategory(int superCategoryId,int index, int numberOfRecordsPerPage);
    }
}
