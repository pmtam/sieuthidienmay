﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Brands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Interfaces
{
    public interface IBrandService: IService<Brand,int>
    {

    }
}
