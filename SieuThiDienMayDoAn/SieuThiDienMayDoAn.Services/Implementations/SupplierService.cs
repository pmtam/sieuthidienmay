﻿using SieuThiDienMayDoAn.Model.Suppliers;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class SupplierService:AbstractService<Supplier,int>,ISupplierService
       
    {
        ISupplierRepository _supplierRepository;
        public SupplierService(ISupplierRepository supplierRepository)
            : base(supplierRepository)
        { _supplierRepository = supplierRepository; }
    }
}
