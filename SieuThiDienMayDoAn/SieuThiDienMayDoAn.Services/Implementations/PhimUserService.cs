﻿

using SieuThiDienMayDoAn.Model.PhimUsers;
using SieuThiDienMayDoAn.Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Service.Services
{
    public class PhimUserService:AbstractService<PhimUser,int>,  IUserService
    {
        IUserRepository _userRepository;
        public PhimUserService(IUserRepository userRepository)
            : base(userRepository)
        { _userRepository = userRepository; }
    }
}
