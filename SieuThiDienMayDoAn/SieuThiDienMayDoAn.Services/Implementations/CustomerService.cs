﻿using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.CustomerService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Services.Mapping;
using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Services.ViewModels;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IDeliveryAddressRepository _deliveryAddressRepository;
        private readonly IUnitOfWork _uow;

        public CustomerService(ICustomerRepository customerRepository, IUnitOfWork uow,IDeliveryAddressRepository deliveryAddressRepository)
        {
            _customerRepository = customerRepository;
            _uow = uow;
            _deliveryAddressRepository = deliveryAddressRepository;
        }

        public CreateCustomerResponse CreateCustomer(CreateCustomerRequest request)
        {
            CreateCustomerResponse response = new CreateCustomerResponse();
            Customer customer = new Customer();
            customer.UserName = request.UserName;
            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Phone = request.Phone;
            ThrowExceptionIfCustomerIsInvalid(customer);

            _customerRepository.Add(customer);
            _uow.Commit();
            response.Customer = customer.ConvertToCustomerDetailView();
            return response;

        }

        private void ThrowExceptionIfCustomerIsInvalid(Customer customer)
        {
            if (customer.GetBrokenRules().Count() > 0)
            {
                StringBuilder brokenRules = new StringBuilder();
                brokenRules.AppendLine("There were problems saving the Customer:");
                foreach (BusinessRule businessRule in customer.GetBrokenRules())
                {
                    brokenRules.AppendLine(businessRule.Rule);
                }
                throw new CustomerInvalidException(brokenRules.ToString());
            }
        }

        public GetCustomerResponse GetCustomer(GetCustomerRequest request)
        {
            GetCustomerResponse response = new GetCustomerResponse();
            
            Customer customer = _customerRepository.FindBy(request.CustomerIdentityToken);
            if (customer != null)
            {
                response.CustomerFound = true;
                response.Customer = customer.ConvertToCustomerDetailView();
                if (request.LoadOrderSummary)
                    response.Orders = customer.Orders
                    .OrderByDescending(o => o.Created).ConvertToOrderSummaryViews();
            }
            else
                response.CustomerFound = false;
            
            return response;
        }



        public ModifyCustomerResponse ModifyCustomer(ModifyCustomerRequest request)
        {
            ModifyCustomerResponse response = new ModifyCustomerResponse();
            Customer customer = _customerRepository.FindBy(request.CustomerIdentityToken);
            customer.FirstName = request.FirstName;
            customer.UserName = request.SecondName;
            customer.Email = request.Email;
            ThrowExceptionIfCustomerIsInvalid(customer);

            _customerRepository.Save(customer);
            _uow.Commit();
            response.Customer = customer.ConvertToCustomerDetailView();
            return response;
        }

        public DeliveryAddressModifyResponse ModifyDeliveryAddress(DeliveryAddressModifyRequest request)
        {
            DeliveryAddressModifyResponse response =new DeliveryAddressModifyResponse();
            Customer customer = _customerRepository.FindBy(request.CustomerIdentityToken);
            DeliveryAddress deliveryAddress = customer.DeliveryAddressBook.
                Where(d => d.Id == request.Address.Id).FirstOrDefault();
            if (deliveryAddress != null)
            {
                UpdateDeliveryAddressFrom(request.Address, deliveryAddress);
                _customerRepository.Save(customer);
                _uow.Commit();
            }

            response.DeliveryAddress = deliveryAddress.ConvertToDeliveryAddressView();
            return response;
        }

        public DeliveryAddressAddResponse AddDeliveryAddress(DeliveryAddressAddRequest request)
        {
            DeliveryAddressAddResponse response = new DeliveryAddressAddResponse();
            Customer customer = _customerRepository.FindBy(request.UserName);
            DeliveryAddress deliveryAddress = new DeliveryAddress();
            deliveryAddress.Customer = customer;
            UpdateDeliveryAddressFrom(request.Address, deliveryAddress);

            customer.AddAddress(deliveryAddress);
            _customerRepository.Save(customer);
            _uow.Commit();
            response.DeliveryAddress = deliveryAddress.ConvertToDeliveryAddressView();
            return response;
        }

        private void UpdateDeliveryAddressFrom(DeliveryAddressView deliveryAddressSource, DeliveryAddress deliveryAddressToUpdate)
        {
            deliveryAddressToUpdate.NumberOfHouse = deliveryAddressSource.NumberOfHouse;
            deliveryAddressToUpdate.Street = deliveryAddressSource.Street;
            
            deliveryAddressToUpdate.Ward= deliveryAddressSource.Ward;
            deliveryAddressToUpdate.District = deliveryAddressSource.District;
            deliveryAddressToUpdate.Province = deliveryAddressSource.province;

        }

        public IEnumerable<DeliveryAddressView> GetDeliveryAddresses(int customerId)
        {
            return _deliveryAddressRepository.GetDeliveryAddressesByCustomer(customerId).ConvertToDeliveryAddressesView();
        }

        public Customer FindBy(string userName)
        {
            return _customerRepository.FindBy(userName);
        }

    }
}
