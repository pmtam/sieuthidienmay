﻿
using SieuThiDienMayDoAn.Model.Ves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class VeService:AbstractService<Ve,int>,  IVeService
    {
        IVeRepository _veRepository;
        public VeService(IVeRepository veRepository)
            : base(veRepository)
        { _veRepository = veRepository; }
    }
}
