﻿using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class CategoryService:AbstractService<Category,int>,ICategoryService
       
    {
        ICategoryRepository _categoryRepository;
        public CategoryService(ICategoryRepository categoryRepository)
            : base(categoryRepository)
        { _categoryRepository = categoryRepository; }

        
    }
}
