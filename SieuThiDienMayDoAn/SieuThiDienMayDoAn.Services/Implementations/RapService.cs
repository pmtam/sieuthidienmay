﻿
using SieuThiDienMayDoAn.Model.Raps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class RapService:AbstractService<Rap,int>,  IRapService
    {
        IRapRepository _rapRepository;
        public RapService(IRapRepository rapRepository)
            : base(rapRepository)
        { _rapRepository = rapRepository; }


        public List<Rap> GetRapsByPhim(int phimId)
        {
            return _rapRepository.GetRapsByPhim(phimId);
        }
        public Rap GetRapsByPhimAndRap(int phimId, int rapId)
        {
            return _rapRepository.GetRapsByPhimAndRap(phimId, rapId);
        }
    }
}
