﻿using SieuThiDienMayDoAn.Infrastructure.Logging;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Basket;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Model.Orders;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Services.Mapping;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IBasketRepository _basketRepository;
        private readonly IDeliveryAddressService _deliveryAddressService;
        private readonly IUnitOfWork _uow;
        public OrderService(IOrderRepository orderRepository,
        IBasketRepository basketRepository,
        ICustomerRepository customerRepository,IDeliveryAddressService deliveryAddressService,
        IUnitOfWork uow)
        {
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _basketRepository = basketRepository;
            _deliveryAddressService = deliveryAddressService;
            _uow = uow;
        }
        public CreateOrderResponse CreateOrder(CreateOrderRequest request)
        {
            CreateOrderResponse response = new CreateOrderResponse();
            Customer customer = _customerRepository
                .FindBy(request.UserName);
            Basket basket = _basketRepository.FindBy(new Guid(request.BasketId));
            //DeliveryAddress deliveryAddress = _deliveryAddressService.FindBy(request.DeliveryAddressId);
            Order order = basket.ConvertToOrder();
            order.Customer = customer;

            //order.ShippingCharge = request.ShippingCost;
            
            order.DeliveryAddressId = request.DeliveryAddressId;
            _orderRepository.Save(order);
            _basketRepository.Remove(basket);
            _uow.Commit();
            response.Order = order.ConvertToOrderView();
            return response;
        }

        public GetOrderResponse GetOrder(GetOrderRequest request)
        {
            GetOrderResponse response = new GetOrderResponse();
            Order order = _orderRepository.FindBy(request.OrderId);
            response.Order = order.ConvertToOrderView();
            return response;
        }
    }

}
