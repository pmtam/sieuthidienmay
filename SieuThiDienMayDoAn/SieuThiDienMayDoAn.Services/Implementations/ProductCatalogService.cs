﻿using SieuThiDienMayDoAn.Infrastructure.Querying;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Services.Mapping;
using SieuThiDienMayDoAn.Services.ViewModels;
using SieuThiDienMayDoAn.Model.SuperCategories;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class ProductCatalogService : IProductCatalogService
    {
       
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ISuperCategoryRepository _superCategoryRepository;



        public ProductCatalogService(
            IProductRepository productRepository,ICategoryRepository categoryRepository, ISuperCategoryRepository superCategoryRepository)
        {
            
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _superCategoryRepository = superCategoryRepository;
        }

        private IEnumerable<Product> GetAllProductsMatchingQueryAndSort(GetProductsByCategoryRequest request,
            Query productQuery)
        {

            IEnumerable<Product> productsMatchingRefinement =_productRepository.FindBy(productQuery);
            switch (request.SortBy)
            {
                case ProductsSortBy.PriceLowToHigh:
                    productsMatchingRefinement = productsMatchingRefinement
                    .OrderBy(p => p.Price);
                    break;
                case ProductsSortBy.PriceHighToLow:
                    productsMatchingRefinement = productsMatchingRefinement
                    .OrderByDescending(p => p.Price);
                    break;
            }
            return productsMatchingRefinement;
        }



        public GetProductsByCategoryResponse GetProductsByCategory(GetProductsByCategoryRequest request)
        {
            GetProductsByCategoryResponse response;

            Query productQuery = ProductSearchRequestQueryGenerator.CreateQueryFor(request);

            IEnumerable<Product> productsMatchingRefinement = GetAllProductsMatchingQueryAndSort(request, productQuery);
            int count = productsMatchingRefinement.Count();

            response = productsMatchingRefinement.CreateProductSearchResultFrom(request);



            /*response.SelectedCategoryName =
                _categoryRepository.FindBy(request.CategoryId).Name;*/


            return response;
        }

        public IEnumerable<ProductSummaryView> GetProductsBySuperCategory(int superCategoryId,int index, int numberOfRecordsPerPage)
        {
            List<Product> productsResult = new List<Product>();
            SuperCategory superCategory = _superCategoryRepository.FindBy(superCategoryId);

            IEnumerable<Category> categorys = superCategory.Categories;
            foreach(Category category in categorys)
            {
                IEnumerable<Product> products = _productRepository.GetProductByCategory(category.Id);
                productsResult.AddRange(products);
            }
            
            return productsResult.Skip(index*numberOfRecordsPerPage).Take(numberOfRecordsPerPage).ConvertToProductViews();
        }
    }
}
