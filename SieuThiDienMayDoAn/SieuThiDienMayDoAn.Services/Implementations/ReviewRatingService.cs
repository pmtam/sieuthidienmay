﻿using SieuThiDienMayDoAn.Infrastructure.Querying;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Model.Review;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ReviewRatingService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Services.Mapping;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class ReviewRatingService :IReviewRatingService
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly ICommentRepository _commentRepository;


        public ReviewRatingService(
            IProductRepository productRepository,ICategoryRepository categoryRepository,IReviewRepository reviewRepository,ICommentRepository commentRepository)
        {
            
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _reviewRepository = reviewRepository;
            _commentRepository = commentRepository;
        }


        public GetAllReviewByProductResponse GetAllReviewByProduct(GetAllReviewByProductRequest getAllReviewByProductRequest)
        {
            GetAllReviewByProductResponse getAllReviewByProductResponse = new GetAllReviewByProductResponse()
            {
                ReviewViews = _reviewRepository.GetReviewsByProduct(getAllReviewByProductRequest.ProductId).ConvertToReviewViews()
            };

            return getAllReviewByProductResponse;
            
        }

        public GetAllReviewByProductResponse GetAllReviewByProductWithPaging(GetAllReviewByProductRequest getAllReviewByProductRequest, int index, int count)
        {
            GetAllReviewByProductResponse getAllReviewByProductResponse = new GetAllReviewByProductResponse()
            {
                ReviewViews = _reviewRepository.GetReviewsByProduct(getAllReviewByProductRequest.ProductId,index,count).ConvertToReviewViews()
            };

            return getAllReviewByProductResponse;
        }


        public IEnumerable<CommentView> GetCommentsByReview(int reviewId)
        {
            return _commentRepository.GetCommentsByReview(reviewId).ConvertToCommentViews();
        }
        public IEnumerable<CommentView> GetCommentsByReview(int reviewId, int index, int count)
        {
            return _commentRepository.GetCommentsByReview(reviewId,index,count).ConvertToCommentViews();
        }

    }
}
