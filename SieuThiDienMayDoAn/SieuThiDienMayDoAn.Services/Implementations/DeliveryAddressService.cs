﻿using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class DeliveryAddressService : AbstractService<DeliveryAddress, int>, IDeliveryAddressService
    {
        IDeliveryAddressRepository _deliveryAddressRepository;
        public DeliveryAddressService(IDeliveryAddressRepository deliveryAddressRepository)
            : base(deliveryAddressRepository)
        { _deliveryAddressRepository =deliveryAddressRepository; }
    }
}
