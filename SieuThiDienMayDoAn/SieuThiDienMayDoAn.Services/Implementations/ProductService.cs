﻿using SieuThiDienMayDoAn.Infrastructure.Querying;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ProductService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Services.Mapping;
using SieuThiDienMayDoAn.Services.ViewModels;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class ProductService:AbstractService<Product,int>,IProductService
       
    {
        IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
            : base(productRepository)
        { _productRepository = productRepository; }

        public GetFeaturedProductsResponse GetFeaturedProducts()
        {
            GetFeaturedProductsResponse response = new GetFeaturedProductsResponse();
            Query productQuery = new Query();

            productQuery.OrderByProperty = new OrderByClause() { Desc = true, PropertyName = PropertyNameHelper.ResolvePropertyName<Product>(p => p.Price) };
            response.Products = FindBy(productQuery, 0, 6).ConvertToProductViews();

            return response;

        }

        public GetLatestProductsResponse GetLatestProducts()
        {
            GetLatestProductsResponse response = new GetLatestProductsResponse();
            Query productQuery = new Query();

            productQuery.OrderByProperty = new OrderByClause() { Desc = true, PropertyName = PropertyNameHelper.ResolvePropertyName<Product>(p => p.ImportDate) };

            
            
            response.Products = FindBy(productQuery, 0, 10).ConvertToProductViews();

            return response;

        }

        public GetBestSellerProductsResponse GetBestSellerProducts()
        {
            GetBestSellerProductsResponse response = new GetBestSellerProductsResponse();
            Query productQuery = new Query();

            productQuery.OrderByProperty = new OrderByClause() { Desc = true, PropertyName = PropertyNameHelper.ResolvePropertyName<Product>(p => p.SoldQuantity) };
            response.Products = FindBy(productQuery, 0, 20).ConvertToProductViews();

            return response;

        }
        public GetSpecialProductsResponse GetSpecialProducts()
        {
            GetSpecialProductsResponse response = new GetSpecialProductsResponse();
            Query productQuery = new Query();

            productQuery.OrderByProperty = new OrderByClause() { Desc = true, PropertyName = PropertyNameHelper.ResolvePropertyName<Product>(p => p.Discount) };
            response.Products = FindBy(productQuery, 0, 20).ConvertToProductViews();

            return response;

        }

        

        public GetProductResponse GetProduct(GetProductRequest request)
        {
            GetProductResponse response = new GetProductResponse();
            response.Product = FindBy(request.ProductId).ConvertToProductView();
            return response;
        }

        public IEnumerable<ProductSummaryView> SearchProducts(string searchStr, int index, int numberOfRecordsPerPage)
        {
            return _productRepository.SearchProducts(searchStr,index,numberOfRecordsPerPage).ConvertToProductViews();
 
        }

    }
}
