﻿
using SieuThiDienMayDoAn.Model.ChiTietDatVes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class ChiTietDatVeService:AbstractService<ChiTietDatVe,int>,  IChiTietDatVeService
    {
        IChiTietDatVeRepository _chiTietDatVeRepository;
        public ChiTietDatVeService(IChiTietDatVeRepository chiTietDatVeRepository)
            : base(chiTietDatVeRepository)
        { _chiTietDatVeRepository = chiTietDatVeRepository; }
    }
}
