﻿
using SieuThiDienMayDoAn.Model.DatVes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class DatVeService:AbstractService<DatVe,int>,  IDatVeService
    {
        IDatVeRepository _datVeRepository;
        public DatVeService(IDatVeRepository datVeRepository)
            : base(datVeRepository)
        { _datVeRepository = datVeRepository; }
    }
}
