﻿using SieuThiDienMayDoAn.Model.Brands;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class BrandService:AbstractService<Brand,int>,IBrandService
       
    {
        IBrandRepository _brandRepository;
        public BrandService(IBrandRepository brandRepository)
            : base(brandRepository)
        { _brandRepository = brandRepository; }
    }
}
