﻿
using SieuThiDienMayDoAn.Model.Phims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class PhimService:AbstractService<Phim,int>,  IPhimService
    {
        IPhimRepository _phimRepository;
        public PhimService(IPhimRepository phimRepository)
            : base(phimRepository)
        { _phimRepository = phimRepository; }

        public IEnumerable<Phim> searchPhim(string searchStr)
        {
            return _phimRepository.searchPhim(searchStr);
        }
    }
}
