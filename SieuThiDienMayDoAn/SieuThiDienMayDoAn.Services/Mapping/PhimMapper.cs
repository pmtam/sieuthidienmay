﻿using AutoMapper;
using SieuThiDienMayDoAn.Model.Raps;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Mapping
{
    public static class PhimMapper
    {
        public static RapView ConvertToRapView(this Rap rap)
        {
            return Mapper.Map<Rap, RapView>(rap);
        }

        public static IEnumerable<RapView> ConvertToRapViews(
                                                  this IEnumerable<Rap> raps)
        {
            return Mapper.Map<IEnumerable<Rap>,
                              IEnumerable<RapView>>(raps);
        }
    }
}
