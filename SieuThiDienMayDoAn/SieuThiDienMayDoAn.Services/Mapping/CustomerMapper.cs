﻿using AutoMapper;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Mapping
{
    public static  class CustomerMapper
    {
        public static CustomerView ConvertToCustomerDetailView(this Customer customer)
        {
            return Mapper.Map<Customer, CustomerView>(customer);
        }
    }
}
