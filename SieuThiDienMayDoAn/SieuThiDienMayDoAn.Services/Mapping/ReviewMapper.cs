﻿using AutoMapper;
using SieuThiDienMayDoAn.Model.Review;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Mapping
{
    public static class ReviewMapper
    {
        public static ReviewView ConvertToReviewView(this Review review)
        {
            return Mapper.Map<Review, ReviewView>(review);
        }

        public static IEnumerable<ReviewView> ConvertToReviewViews(
                                                  this IEnumerable<Review> reviews)
        {
            return Mapper.Map<IEnumerable<Review>,
                              IEnumerable<ReviewView>>(reviews);
        }


        public static CommentView ConvertToCommentView(this Comment Comment)
        {
            return Mapper.Map<Comment, CommentView>(Comment);
        }

        public static IEnumerable<CommentView> ConvertToCommentViews(
                                                  this IEnumerable<Comment> Comments)
        {
            return Mapper.Map<IEnumerable<Comment>,
                              IEnumerable<CommentView>>(Comments);
        }

    }
}
