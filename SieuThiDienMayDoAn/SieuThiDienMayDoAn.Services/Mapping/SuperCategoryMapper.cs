﻿using AutoMapper;
using SieuThiDienMayDoAn.Model.SuperCategories;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Mapping
{
    public static class SuperCategoryMapper
    {
        public static IEnumerable<SuperCategoryView> ConvertToSuperCategoryViews(
        this IEnumerable<SuperCategory> superCategories)
        {

            return Mapper.Map<IEnumerable<SuperCategory>,
            IEnumerable<SuperCategoryView>>(superCategories);
        }
    }
}
