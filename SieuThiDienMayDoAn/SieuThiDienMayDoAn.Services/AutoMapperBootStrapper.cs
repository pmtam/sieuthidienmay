﻿using AutoMapper;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Infrastructure.Helpers;
using SieuThiDienMayDoAn.Model.SuperCategories;
using SieuThiDienMayDoAn.Model.Shipping;
using SieuThiDienMayDoAn.Model.Basket;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Model.Review;
using SieuThiDienMayDoAn.Model.Orders;
using SieuThiDienMayDoAn.Model;
using SieuThiDienMayDoAn.Model.Orders.States;
using SieuThiDienMayDoAn.Model.Raps;

namespace SieuThiDienMayDoAn.Services
{
    public class AutoMapperBootStrapper
    {
        public static void ConfigureAutoMapper()
        {
            
            Mapper.CreateMap<Product, ProductSummaryView>();
            Mapper.CreateMap<Product, ProductView>();

            //Review
            Mapper.CreateMap<Review, ReviewView>()
    .ForMember(s => s.Id, c => c.MapFrom(m => m.Id))
    .ForMember(s => s.Name, c => c.MapFrom(m => m.Name))
    .ForMember(s => s.Content, c => c.MapFrom(m => m.Content))
    .ForMember(s => s.Rating, c => c.MapFrom(m => m.Rating))
    .ForMember(s => s.ProductId, c => c.MapFrom(m => m.Product.Id))
    .ForMember(s => s.numberOfComments, c => c.MapFrom(m => m.Comments.Count));

            //Comment
            Mapper.CreateMap<Comment, CommentView>();

            // Category
            
            Mapper.CreateMap<Category, CategoryView>()
    .ForMember(s => s.Id, c => c.MapFrom(m => m.Id))
    .ForMember(s => s.Name, c => c.MapFrom(m => m.Name))
    .ForMember(s => s.ProductSumaryViews, c => c.MapFrom(m => m.Products));
           
            //SuperCategory
            Mapper.CreateMap<SuperCategory, SuperCategoryView>();


            Mapper.CreateMap<SuperCategory, SuperCategoryView>()
    .ForMember(s => s.Id, c => c.MapFrom(m => m.Id))
    .ForMember(s => s.Name, c => c.MapFrom(m => m.Name))
    .ForMember(s => s.CategoryViews, c => c.MapFrom(m => m.Categories));
            
            // Global Money Formatter
            //Mapper.AddFormatter<MoneyFormatter>();

            // Basket
            Mapper.CreateMap<DeliveryOption, DeliveryOptionView>();
            Mapper.CreateMap<BasketItem, BasketItemView>();
            Mapper.CreateMap<Basket, BasketView>();

            // Customer
            Mapper.CreateMap<Customer, CustomerView>();
            Mapper.CreateMap<DeliveryAddress, DeliveryAddressView>();
            //Rap
            // Customer
            Mapper.CreateMap<Rap, RapView>();

            // Orders
            Mapper.CreateMap<Order, OrderView>();
            Mapper.CreateMap<OrderItem, OrderItemView>();
            Mapper.CreateMap<Address, DeliveryAddressView>();
            Mapper.CreateMap<Order, OrderSummaryView>()
            .ForMember(o => o.IsSubmitted,
            ov => ov.ResolveUsing<OrderStatusResolver>());

        }

        public class OrderStatusResolver : ValueResolver<Order, bool>
        {
            protected override bool ResolveCore(Order source)
            {
                if (source.Status == OrderStatus.Submitted)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public class MoneyFormatter : IValueFormatter
        {
            public string FormatValue(ResolutionContext context)
            {
                if (context.SourceValue is decimal)
                {
                    decimal money = (decimal)context.SourceValue;
                    return money.FormatMoney();
                }
                return context.SourceValue.ToString();
            }
        }
    }
}
