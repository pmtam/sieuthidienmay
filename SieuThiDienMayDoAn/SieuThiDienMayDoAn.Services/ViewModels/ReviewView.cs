﻿using SieuThiDienMayDoAn.Model.Review;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.ViewModels
{
    public class ReviewView
    {

        public int Id { get; set; }

        public string Name { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
        public int ProductId { get; set; }
        public int numberOfComments { get; set; }

    }
}
