﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.ViewModels
{
    public class CommentView
    {
        public string Content { get; set; }
        public string UserName { get; set; }

    }
}
