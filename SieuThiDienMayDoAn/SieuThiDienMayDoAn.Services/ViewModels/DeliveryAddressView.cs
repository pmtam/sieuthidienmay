﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.ViewModels
{
    public class DeliveryAddressView
    {
        public int Id { get; set; }
        public string NumberOfHouse { get; set; }
        public string Street { get; set; }
        public string Ward { get; set; }
        public string District { get; set; }
        public string province { get; set; }
       
    }
}
