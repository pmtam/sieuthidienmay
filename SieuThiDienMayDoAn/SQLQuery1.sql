﻿CREATE TABLE [dbo].[PHIM] (
    [PhimId]       INT            IDENTITY (1, 1) NOT NULL,
    [Image]        NVARCHAR (MAX) NOT NULL,
    [TenPhim]      NVARCHAR (MAX) NOT NULL,
    [KhoiChieu]    NVARCHAR (MAX) NULL,
    [TheLoai]      NVARCHAR (MAX) NULL,
    [DienVien]     NVARCHAR (MAX) NULL,
    [DaoDien]      NVARCHAR (MAX) NULL,
    [DoDai]        NVARCHAR (MAX) NULL,
    [PhienBan]     NVARCHAR (MAX) NULL,
    [Hang]         NVARCHAR (MAX) NULL,
    [NuocSanXuat]  NVARCHAR (MAX) NULL,
    [MoTa]         NVARCHAR (MAX) NOT NULL,
    [DangChieu]    BIT            NOT NULL,
    [Link]         NVARCHAR (MAX) NULL,
    [OriginalName] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_PHIM] PRIMARY KEY CLUSTERED ([PhimId] ASC)
);

CREATE TABLE [dbo].[RAP] (
    [RapId]     INT             IDENTITY (1, 1) NOT NULL,
    [TenRap]    NVARCHAR (50)   NOT NULL,
    [DiaChi]    NVARCHAR (MAX)  NULL,
    [Image]     NVARCHAR (MAX)  NULL,
    [MaRap]     NVARCHAR (50)   NULL,
    [DienThoai] NVARCHAR (50)   NULL,
    [PhimId]    INT             NOT NULL,
    [LichChieu] NVARCHAR (1000) NULL,
    CONSTRAINT [PK_RAP] PRIMARY KEY CLUSTERED ([RapId] ASC),
    CONSTRAINT [FK_RAP_PHIM] FOREIGN KEY ([PhimId]) REFERENCES [dbo].[PHIM] ([PhimId])
);



CREATE TABLE [dbo].[Ve] (
    [VeId] INT           IDENTITY (1, 1) NOT NULL,
    [Loai] NVARCHAR (50) NOT NULL,
    [Gia]  DECIMAL (18)  NOT NULL,
    CONSTRAINT [PK_Ve] PRIMARY KEY CLUSTERED ([VeId] ASC)
);



CREATE TABLE [dbo].[DATVE] (
    [DatVeId]    INT      IDENTITY (1, 1) NOT NULL,
    [UserName] NVARCHAR(100)      NOT NULL,
    [NgayDatVe]  DATETIME NOT NULL,
    CONSTRAINT [PK_DATVE] PRIMARY KEY CLUSTERED ([DatVeId] ASC),
);

CREATE TABLE [dbo].[CHITIETDATVE] (
    [ChiTietDatVeId] INT             IDENTITY (1, 1) NOT NULL,
    [DatVeId]        INT             NOT NULL,
    [SoLuong]        INT             NOT NULL,
    [TongGia]        DECIMAL (18, 2) NOT NULL,
    [VeId]           INT             NOT NULL,
    CONSTRAINT [PK_CHITIETDATVE] PRIMARY KEY CLUSTERED ([ChiTietDatVeId] ASC),
    CONSTRAINT [FK_CHITIETDATVE_Ve] FOREIGN KEY ([VeId]) REFERENCES [dbo].[Ve] ([VeId]),
    CONSTRAINT [FK_CHITIETDATVE_DATVE] FOREIGN KEY ([DatVeId]) REFERENCES [dbo].[DATVE] ([DatVeId])
);

