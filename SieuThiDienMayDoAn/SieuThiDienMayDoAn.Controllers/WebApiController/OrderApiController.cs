﻿using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.OrderService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SieuThiDienMayDoAn.Controllers.WebApiController
{
    [Authorize]
    public class OrderApiController:ApiController
    {
        IOrderService _orderService;
        public OrderApiController(IOrderService orderService)
        {
            this._orderService = orderService;
        }

        [HttpPost]
        public string CreateOrder(CreateOrderRequest createOrderRequest)
        {
            try
            {
                CreateOrderResponse createOrderResponse =
                 _orderService.CreateOrder(createOrderRequest);
                StringBuilder strBuilder = new StringBuilder();
                strBuilder.Append("Đặt hàng thành công!");
                
                return strBuilder.ToString();
            }
            catch (Exception e)
            {
                return "Đặt hàng thất bại với các lỗi sau:\n" + e.Message;
            }

        }
    }
}
