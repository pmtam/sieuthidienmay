﻿using SieuThiDienMayDoAn.Model.Shipping;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SieuThiDienMayDoAn.Controllers.WebApiController
{
    public class ShippingApiController:ApiController
    {
        private IShippingService _shippingService;
        public ShippingApiController(IShippingService shippingService)
        {
            this._shippingService = shippingService;
        }

        public IEnumerable<DeliveryOption> GetAllDeliveryOptions()
        {
            return _shippingService.FindAll();
        }
    }
}
