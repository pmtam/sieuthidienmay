﻿using SieuThiDienMayDoAn.Controllers.JsonDTOs;
using SieuThiDienMayDoAn.Controllers.ViewModels;
using SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog;
using SieuThiDienMayDoAn.Infrastructure.CookieStorage;
using SieuThiDienMayDoAn.Model.Basket;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.BasketService;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SieuThiDienMayDoAn.Controllers.WebApiController
{
    public class BasketApiController:ApiController
    {
        private readonly IBasketService _basketService;
        private readonly ICookieStorageService _cookieStorageService;

        public BasketApiController(IBasketService basketService,ICookieStorageService cookieStorageService)
        {
            this._basketService = basketService;
            this._cookieStorageService = cookieStorageService;
        }

        public IEnumerable<BasketItemView> GetBasket(string basketId)
        {
            BasketDetailView basketView = new BasketDetailView();
            Guid basketGuid = new Guid(basketId);

            GetBasketRequest basketRequest = new GetBasketRequest() { BasketId = basketGuid };
            GetBasketResponse basketResponse =
                            _basketService.GetBasket(basketRequest);
            return basketResponse.Basket.Items;


        }



        [HttpGet]
        public BasketView RemoveItem(string basketId,int productId)
        {
            ModifyBasketRequest request = new ModifyBasketRequest();
            request.ItemsToRemove.Add(productId);
            request.BasketId = new Guid(basketId) ;

            ModifyBasketResponse response = _basketService.ModifyBasket(request);

            SaveBasketSummaryToCookie(response.Basket.NumberOfItems,
                                      response.Basket.BasketTotal);

            BasketDetailView basketDetailView = new BasketDetailView();

            basketDetailView.Basket = response.Basket;
            
            return response.Basket;
        }

        [HttpGet]
        public UpdateBasketItemsResponse UpdateItems(string basketId, int productId, int qty)
        {
            JsonBasketItemUpdateRequest jsonBasketItemUpdateRequest = new JsonBasketItemUpdateRequest()
            {
                ProductId=productId,
                Qty=qty
            };

            ModifyBasketRequest request = new ModifyBasketRequest();
            request.BasketId = new Guid(basketId);
            JsonBasketQtyUpdateRequest jsonBasketQtyUpdateRequest = new JsonBasketQtyUpdateRequest() 
            { 
                 Items = new JsonBasketItemUpdateRequest[]{jsonBasketItemUpdateRequest}
            };
            
            request.ItemsToUpdate =  jsonBasketQtyUpdateRequest
                                           .ConvertToBasketItemUpdateRequests(); ;

            ModifyBasketResponse reponse = _basketService.ModifyBasket(request);

            SaveBasketSummaryToCookie(reponse.Basket.NumberOfItems,
                                      reponse.Basket.BasketTotal);

            

            UpdateBasketItemsResponse updateBasketItemsResponse = new UpdateBasketItemsResponse()
            {
                BasketTotal = reponse.Basket.BasketTotal,
                NumberOfItems = reponse.Basket.NumberOfItems
            };

            return updateBasketItemsResponse;
            
        }


        private void SaveBasketIdToCookie(Guid basketId)
        {
            _cookieStorageService.Save(CookieDataKeys.BasketId.ToString(),
                                       basketId.ToString(), DateTime.Now.AddDays(1));
        }

        private void SaveBasketSummaryToCookie(int numberOfItems,
                                               string basketTotal)
        {
            _cookieStorageService.Save(CookieDataKeys.BasketItems.ToString(),
                                      numberOfItems.ToString(), DateTime.Now.AddDays(1));
            _cookieStorageService.Save(CookieDataKeys.BasketTotal.ToString(),
                                       basketTotal.ToString(), DateTime.Now.AddDays(1));
        }
    }
}
