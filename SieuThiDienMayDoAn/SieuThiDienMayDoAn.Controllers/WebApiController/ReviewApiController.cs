﻿using SieuThiDienMayDoAn.Infrastructure.Configuration;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ReviewRatingService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace SieuThiDienMayDoAn.Controllers.WebApiController
{
    public class ReviewApiController: ApiController
    {
        IReviewRatingService _reviewRatingService;

        public ReviewApiController(IReviewRatingService reviewRatingService)
        {
            _reviewRatingService = reviewRatingService;
        }


        public IEnumerable<ReviewView> GetAllReviews(int id,int index)
        {
            GetAllReviewByProductRequest getAllReviewByProductRequest = new GetAllReviewByProductRequest();
            getAllReviewByProductRequest.ProductId = id;
            return _reviewRatingService.GetAllReviewByProductWithPaging(getAllReviewByProductRequest,index,int.Parse(ApplicationSettingsFactory
                .GetApplicationSettings().NumberOfResultsPerPage)).ReviewViews;
        }

        public IEnumerable<CommentView> GetCommentsByReview(int id, int index)
        {
            return _reviewRatingService.GetCommentsByReview(id, index, int.Parse(ApplicationSettingsFactory
                .GetApplicationSettings().NumberOfResultsPerPage));
        }

    }
}
