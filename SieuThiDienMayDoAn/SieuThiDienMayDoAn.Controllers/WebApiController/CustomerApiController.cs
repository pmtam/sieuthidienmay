﻿using SieuThiDienMayDoAn.Controllers.ViewModels.CustomerAccount;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.CustomerService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using SieuThiDienMayDoAn.Services.Mapping;

namespace SieuThiDienMayDoAn.Controllers.WebApiController
{
    [Authorize]
    public class CustomerApiController : ApiController
    {

        public ICustomerService _customerService;
        public CustomerApiController(ICustomerService customerService)
        {
            this._customerService = customerService;
        }

        [HttpPost]
        public CustomerView Create(CreateCustomerRequest request)
        {
            Customer customer = _customerService.FindBy(request.UserName);
            if (customer != null)
            {

                return customer.ConvertToCustomerDetailView();

            }

            CreateCustomerResponse response = _customerService.CreateCustomer(request);

            return response.Customer;
        }

        public DeliveryAddressView CreateDeliveryAddress(DeliveryAddressAddRequestDetail deliveryAddressAddRequestDetail)
        {
            DeliveryAddressView deliveryAddressView = new DeliveryAddressView()
            {
                NumberOfHouse = deliveryAddressAddRequestDetail.NumberOfHouse,
                Street = deliveryAddressAddRequestDetail.Street,
                Ward = deliveryAddressAddRequestDetail.Ward,
                District = deliveryAddressAddRequestDetail.District,
                province = deliveryAddressAddRequestDetail.Province
            };
            DeliveryAddressAddRequest deliveryAddressAddRequest = new DeliveryAddressAddRequest()
            {
                UserName = deliveryAddressAddRequestDetail.UserName,
                Address = deliveryAddressView
            };

            return _customerService.AddDeliveryAddress(deliveryAddressAddRequest).DeliveryAddress;
        }

        public IEnumerable<DeliveryAddressView> GetDeliveryAddresses(string userName)
        {
            Customer customer = _customerService.FindBy(userName);
            if (customer != null)
            {
                return _customerService.GetDeliveryAddresses(customer.Id);
            }
            else
            {
                return new List<DeliveryAddressView>();
            }

        }

        public IEnumerable<DeliveryAddressView> GetDeliveryAddresses(int customerId)
        {
            //Customer customer = _customerService.FindBy(userName);

            return _customerService.GetDeliveryAddresses(customerId);

        }

        [HttpGet]
        public Customer FindCustomer(string userName)
        {
            return _customerService.FindBy(userName);
        }

    }
}
