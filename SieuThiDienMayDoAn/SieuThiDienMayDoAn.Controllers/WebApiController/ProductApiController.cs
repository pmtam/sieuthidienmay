﻿using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using SieuThiDienMayDoAn.Services.Mapping;
using SieuThiDienMayDoAn.Services.ViewModels;
using System.Web;
using System.Net;
using System.IO;
using SieuThiDienMayDoAn.Model.Brands;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Model.Suppliers;
using StructureMap;
using SieuThiDienMayDoAn.Services.Implementations;
using SieuThiDienMayDoAn.Repository.NHibernate.Repositories;
using SieuThiDienMayDoAn.Repository.NHibernate;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using SieuThiDienMayDoAn.Infrastructure.Configuration;
using SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog;
using SieuThiDienMayDoAn.Services.Messaging.ProductService;

namespace SieuThiDienMayDoAn.Controllers.WebApiController
{
    public class ProductApiController : ApiController
    {
        IProductService _productService;
        IBrandService _brandService;
        ICategoryService _categoryService;
        ISupplierService _supplierService;
        IProductCatalogService _productCatalogService;
        public ProductApiController(IProductService productService, IBrandService brandService, ICategoryService categoryService,
            ISupplierService supplierService, IProductCatalogService productCatalogService)
        {
            _productService = productService;
            _brandService = brandService;
            _categoryService = categoryService;
            _supplierService = supplierService;
            _productCatalogService = productCatalogService;

        }

        public IEnumerable<ProductSummaryView> GetFeaturedProducts()
        {
            return _productService.GetFeaturedProducts().Products;
        }

        public IEnumerable<ProductSummaryView> GetLatestProducts()
        {
            return _productService.GetLatestProducts().Products;
        }
        public IEnumerable<ProductSummaryView> GetBestSellerProducts()
        {
            return _productService.GetBestSellerProducts().Products;
        }
        public IEnumerable<ProductSummaryView> GetSpecialProducts()
        {
            return _productService.GetSpecialProducts().Products;
        }

        public ProductDetailView GetProductDetail(int id)
        {
            ProductDetailView productDetailView = new ProductDetailView();
            GetProductRequest request = new GetProductRequest() { ProductId = id };
            GetProductResponse response = _productService.GetProduct(request);
            ProductView productView = response.Product;
            productDetailView.Product = productView;
            
            return (productDetailView);
        }

        public IEnumerable<ProductSummaryView> GetProductsByCategory(int categoryId,int index)
        {
            GetProductsByCategoryRequest getProductsByCategoryRequest = new GetProductsByCategoryRequest();
            getProductsByCategoryRequest.CategoryId = categoryId;
            getProductsByCategoryRequest.Index = index;
            getProductsByCategoryRequest.NumberOfResultsPerPage = int.Parse(ApplicationSettingsFactory.GetApplicationSettings().NumberOfResultsPerPage);
            getProductsByCategoryRequest.SortBy = ProductsSortBy.PriceLowToHigh;

            GetProductsByCategoryResponse response = _productCatalogService.GetProductsByCategory(getProductsByCategoryRequest);
            return response.Products;
        }

        public IEnumerable<ProductSummaryView> GetProductsBySuperCategory(int superCategoryId,int index, int numberOfRecordsPerPage)
        {
            return _productCatalogService.GetProductsBySuperCategory(superCategoryId,index,numberOfRecordsPerPage);
        }


        [HttpGet]
        public IEnumerable<ProductSummaryView> SearchProducts(string searchStr,int index)
        {
            return _productService.SearchProducts(searchStr,index,int.Parse(ApplicationSettingsFactory.GetApplicationSettings().NumberOfResultsPerPage));
        }


        public IEnumerable<ProductSummaryView> GetProductsWithPaging(int index, int numberOfRecordsPerPage)
        {
            return _productService.FindAll(index, numberOfRecordsPerPage).ConvertToProductViews();
        }

        public IEnumerable<ProductSummaryView> GetAllProducts()
        {
            return _productService.FindAll().ConvertToProductViews();
        }

        public IEnumerable<Brand> GetAllBrands()
        {
            return _brandService.FindAll();
        }

        public IEnumerable<Category> GetAllCategories()
        {
            return _categoryService.FindAll();
        }

        public IEnumerable<Supplier> GetAllSuppliers()
        {
            return _supplierService.FindAll();
        }

        [HttpPost]
        public async Task<IEnumerable<string>> AddProduct()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/uploads");
            var provider = new MyMultipartFormDataStreamProvider(root);

            try
            {
                StringBuilder sb = new StringBuilder(); // Holds the response body
                List<String> strs = new List<string>();

                // Read the form data and return an async task.
                await Request.Content.ReadAsMultipartAsync(provider);



                // This illustrates how to get the file names for uploaded files.
                foreach (var file in provider.FileData)
                {
                    FileInfo fileInfo = new FileInfo(file.LocalFileName);
                    sb.Append(string.Format("Uploaded file: {0} ({1} bytes)\n", fileInfo.Name, fileInfo.Length));
                    strs.Add(fileInfo.Name);
                }

                // This illustrates how to get the form data.
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        sb.Append(string.Format("{0}: {1}\n", key, val));
                        strs.Add(string.Format("{0}: {1}\n", key, val));
                    }
                }

                //Brand brand = _brandService.FindBy(1);
                string brand = provider.FormData["brand"];
                string category = provider.FormData["category"];
                string supplier = provider.FormData["supplier"];
                string name = provider.FormData["name"];
                string price = provider.FormData["price"];
                string quantity = provider.FormData["quantity"];
                string discount = provider.FormData["discount"];

                string largeImage = provider.FormData["largeImage"];
                string smallImage = provider.FormData["smallImage"];
                string tinyImage = provider.FormData["tinyImage"];

                string importDate = provider.FormData["importDate"];
                string description = provider.FormData["description"];


                Product product = new Product()
                {
                    Description = description,
                    Discount = int.Parse(discount),
                    ImageUrl = largeImage,
                    ImportDate = Convert.ToDateTime(importDate),
                    Name = name,
                    Price = decimal.Parse(price),
                    Quantity = int.Parse(quantity),
                    Rating = 0,
                    SoldQuantity = 0,
                    TypeCode = "Latest",
                    ProductBrand= _brandService.FindBy(int.Parse(brand)),
                    ProductCategory=_categoryService.FindBy(int.Parse(category)),
                    ProductSupplier=_supplierService.FindBy(int.Parse(supplier))

                };

                _productService.Add(product);

                return strs;
            }
            catch (System.Exception e)
            {
                List<string> errors = new List<string>();
                errors.Add("error");
                errors.Add("     Không thể thêm sản phẩm");
                return  errors;
            }
        }



    }


    public class MyMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        public MyMultipartFormDataStreamProvider(string path)
            : base(path)
        {

        }

        public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
        {
            string fileName;
            if (!string.IsNullOrWhiteSpace(headers.ContentDisposition.FileName))
            {
                fileName = headers.ContentDisposition.FileName;
            }
            else
            {
                fileName = Guid.NewGuid().ToString() + ".data";
            }
            return fileName.Replace("\"", string.Empty);
        }

        public string GetRootPath()
        {
            return this.RootPath;
        }

    }
}

