﻿
using SieuThiDienMayDoAn.Controllers.ViewModels;
using SieuThiDienMayDoAn.Controllers.ViewModels.Phim;
using SieuThiDienMayDoAn.Infrastructure.Configuration;
using SieuThiDienMayDoAn.Model.ChiTietDatVes;
using SieuThiDienMayDoAn.Model.DatVes;
using SieuThiDienMayDoAn.Model.Phims;
using SieuThiDienMayDoAn.Model.PhimUsers;
using SieuThiDienMayDoAn.Model.Raps;
using SieuThiDienMayDoAn.Model.Ves;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using SieuThiDienMayDoAn.Services.Mapping;

namespace SieuThiDienMayDoAn.Controllers.WebApiController
{
    public class PhimApiController : ApiController
    {

        public IPhimService _phimService;

        public IRapService _rapService;
        public IUserService _userService;
        public IDatVeService _datveService;
        public IChiTietDatVeService _chiTietDatVeService;
        public IVeService _veService;

        public PhimApiController(IPhimService phimService, IRapService rapService, IUserService userService,
            IDatVeService datVeService, IChiTietDatVeService chiTietDatVeService, IVeService veService)
        {
            _phimService = phimService;
            _rapService = rapService;
            _userService = userService;
            _datveService = datVeService;
            _chiTietDatVeService = chiTietDatVeService;
            _veService = veService;
        }
        // GET api/PhimApi
        public IEnumerable<Phim> GetPhims()
        {
            return _phimService.FindAll();
        }

        public IEnumerable<Phim> GetPhimsWithIndex(int index)
        {
            return _phimService.FindAll(index, int.Parse(ApplicationSettingsFactory.GetApplicationSettings().NumberOfResultsPerPage));
        }


        public IEnumerable<Phim> GetPhimsForIndexPage()
        {
            //return _phimService.FindBy(index, int.Parse(ApplicationSettingsFactory.GetApplicationSettings().NumberOfResultsPerPage));
            IEnumerable<Phim> phims = _phimService.FindAll();
            List<Phim> phimsReult = new List<Phim>();
            var query = phims.GroupBy(p => p.DangChieu);
            foreach (var g in query)
            {
                for (int i = 0; i < (g.Count() > 5 ? 5 : g.Count()); i++)
                {
                    phimsReult.Add(g.ElementAt(i));

                }
            }

            return phimsReult;


        }

        // GET api/PhimApi/5
        public Phim GetPhim(int id)
        {
            Phim phim = _phimService.FindBy(id);
            if (phim == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return phim;

        }

        [HttpPost]
        public string DatVe(DatVeRequest datVeRequest)
        {
            try
            {
                PhimUser customer = new PhimUser()
                {
                    Email = datVeRequest.Email,
                    FirstName = datVeRequest.FirstName,
                    LastName = datVeRequest.LastName,
                    Sdt = datVeRequest.Sdt,
                    UserName = datVeRequest.UserName

                };

                int id = _userService.Add(customer);


                DatVe datve = new DatVe()
                {
                    NgayDatVe = DateTime.Now,
                    UserDatVe = _userService.FindBy(id)
                };

                int datVeId = _datveService.Add(datve);


                ChiTietDatVe chiTietDatVe = new ChiTietDatVe()
                {

                    DatVeInfo = _datveService.FindBy(datVeId),
                    SoLuong = datVeRequest.SoLuongVe,
                    TongGia = datVeRequest.SoLuongVe * 100000,
                    VeInfo = _veService.FindBy(1)
                };


                _chiTietDatVeService.Add(chiTietDatVe);
            }
            catch (Exception e)
            {
                return "Đặt vé thất bại với lỗi sau: " + e.Message;
            }

            return "Đặt vé thành công";
        }

        [HttpGet]
        public IEnumerable<Phim> SearchPhim(string searchStr)
        {
            searchStr = WebUtility.HtmlDecode(searchStr);
            return _phimService.searchPhim(searchStr);
        }

        

        public IEnumerable<RapView> GetRapByPhim(int id)
        {

            List<Rap> raps =  _rapService.GetRapsByPhim(id);
            List<RapView> rapViews = new List<RapView>();
            foreach (Rap r in raps)
            {
                RapView rv = new RapView()
                {
                    Id=r.Id,
                    DiaChi=r.DiaChi,
                    DienThoai=r.DienThoai,
                    LichChieu=r.LichChieu,
                    MaRap=r.MaRap,
                    TenRap =r.TenRap,
                    Image=r.Image
                    
                };
                rapViews.Add(rv);


            }
            return rapViews;
        }

        public IEnumerable<string> GetAllDates()
        {
            var raps = _rapService.FindAll().GroupBy(r => r.TenRap);
            List<LichChieuResponse> lichChieuResponses = new List<LichChieuResponse>();
            foreach (var r in raps)
            {
                Rap rap = r.First();
                string lichchieu = rap.LichChieu;
                lichchieu = lichchieu.Remove(lichchieu.Length - 1);

                if (!lichchieu.Equals(""))
                {
                    string[] strs = lichchieu.Split(new string[] { "|" }, StringSplitOptions.None);

                    if (strs.Length > 1)
                    {
                        for (int i = 0; i < strs.Length; i++)
                        {
                            LichChieuResponse l = new LichChieuResponse();
                            l.NgayChieu = strs[i].Substring(0, strs[i].IndexOf('+'));
                            l.Times = strs[i].Substring(strs[i].IndexOf('+') + 1).Split(',').ToList();
                            lichChieuResponses.Add(l);
                        }

                    }
                }

            }

            var lcGroups = lichChieuResponses.GroupBy(lc => lc.NgayChieu);

            List<string> ngayChieus = new List<string>();
            foreach (var lc in lcGroups)
            {
                LichChieuResponse lcResponse = lc.First();
                ngayChieus.Add(lcResponse.NgayChieu);
            }

            return ngayChieus;

        }

        public List<LichChieuResponse> GetNgaysByPhimAndRap(int phimId, int rapId)
        {
            Rap rap = _rapService.GetRapsByPhimAndRap(phimId, rapId);
            string lichchieu = rap.LichChieu;
            List<LichChieuResponse> lichChieuResponses = new List<LichChieuResponse>();
            if (!lichchieu.Equals(""))
            {
                string[] strs = lichchieu.Split(new string[] { "//" }, StringSplitOptions.None);

                if (strs.Count() > 1)
                {

                    LichChieuResponse first = new LichChieuResponse();
                    //LichChieuResponse second = new LichChieuResponse();

                    first.NgayChieu = strs[0];
                    lichChieuResponses.Add(first);
                    for (int i = 1; i < strs.Count() - 1; i++)
                    {
                        string[] times = strs[i].Split(new string[] { "," }, StringSplitOptions.None);
                        List<string> timesOfNgayChieu = new List<string>();
                        for (int j = 0; j < times.Count() - 1; j++)
                        {
                            timesOfNgayChieu.Add(times[j]);
                        }
                        lichChieuResponses[i - 1].Times = timesOfNgayChieu;
                        LichChieuResponse lichChieuResponse = new LichChieuResponse();
                        lichChieuResponse.NgayChieu = times[times.Count() - 1];
                        lichChieuResponses.Add(lichChieuResponse);
                    }

                    List<string> timesOfLast = new List<string>();
                    string[] timesLast = strs[strs.Count() - 1].Split(new string[] { "," }, StringSplitOptions.None);
                    for (int k = 0; k < timesLast.Count(); k++)
                    {
                        timesOfLast.Add(timesLast[k]);
                    }

                    lichChieuResponses.Last().Times = timesOfLast;
                    
                    
                }
               
            }

            return lichChieuResponses;
        
        }


    }
}
