﻿
using SieuThiDienMayDoAn.Controllers.ViewModels;
using SieuThiDienMayDoAn.Controllers.ViewModels.Phim;
using SieuThiDienMayDoAn.HtmlParser;
using SieuThiDienMayDoAn.Model.Phims;
using SieuThiDienMayDoAn.Model.Raps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SieuThiDienMayDoAn.Controllers.Controllers
{
    public class PhimController : Controller
    {
       
        IPhimService _phimService;
        IRapService _rapService;


        public PhimController(IRapService rapService,IPhimService phimService)
        {
            
            _phimService = phimService;
            _rapService = rapService;
        }
            
        
            


        public ActionResult Index()
        {
            
            
            MegastarHtmlParser mega = new MegastarHtmlParser(_phimService, _rapService);


            mega.GetDanhSachPhim(@"http://www.megastar.vn/vn/nowshowing/",true);
            mega.GetDanhSachPhim(@"http://www.megastar.vn/vn/comingsoon/", false);


            
            return View();
        }

        public ActionResult Detail(int Id)
        {
            Phim phim = _phimService.FindBy(Id);
            return View(phim);
        }


        public ActionResult Search(string phim)
        {

            SearchData search = new SearchData();

            search.SearchStr = WebUtility.HtmlDecode(phim);
            return View(search);
        }


    }
}
