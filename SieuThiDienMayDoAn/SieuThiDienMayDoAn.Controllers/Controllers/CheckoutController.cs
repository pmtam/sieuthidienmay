﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SieuThiDienMayDoAn.Services.Mapping;
using SieuThiDienMayDoAn.Services.ViewModels;
using SieuThiDienMayDoAn.Infrastructure.CookieStorage;
using SieuThiDienMayDoAn.Controllers.ViewModels.Checkout;

namespace SieuThiDienMayDoAn.Controllers.Controllers
{
    public class CheckoutController : ProductCatalogBaseController
    {

         private readonly IProductCatalogService _productCatalogService;
       
        private readonly IProductService _productService;
        private readonly ISuperCategoryService _superCategoryService;
        private readonly ICategoryService _categoryService;

        public CheckoutController(ICookieStorageService cookieStorageService, IProductCatalogService productCatalogService, ICategoryService categoryService, IProductService productService, ISuperCategoryService superCategoryService)
            : base(cookieStorageService,categoryService, superCategoryService)
        {
            _productCatalogService = productCatalogService;

            _categoryService = categoryService;
            _productService = productService;
            _superCategoryService = superCategoryService;
        }

        public ActionResult CheckoutPage()
        {
            CheckoutPageView checkoutPageView = new CheckoutPageView();
            checkoutPageView.Categories = base.GetCategories();
            checkoutPageView.SuperCategories = base.GetSuperCategories();
            checkoutPageView.BasketSummary = base.GetBasketSummaryView();
            checkoutPageView.BasketId = base.GetBasketId();
            return View(checkoutPageView);
        }
    }
}
