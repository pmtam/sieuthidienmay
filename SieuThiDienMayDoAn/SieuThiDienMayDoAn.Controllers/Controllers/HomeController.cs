﻿using SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SieuThiDienMayDoAn.Services.Mapping;
using SieuThiDienMayDoAn.Services.ViewModels;
using SieuThiDienMayDoAn.Infrastructure.CookieStorage;
namespace SieuThiDienMayDoAn.Controllers.Controllers
{
    public class HomeController : ProductCatalogBaseController
    {
        private readonly IProductCatalogService _productCatalogService;
       
        private readonly IProductService _productService;
        private readonly ISuperCategoryService _superCategoryService;
        private readonly ICategoryService _categoryService;

        public HomeController(ICookieStorageService cookieStorageService,IProductCatalogService productCatalogService, ICategoryService categoryService,IProductService productService,ISuperCategoryService superCategoryService)
            : base(cookieStorageService,categoryService, superCategoryService)
        {
            _productCatalogService = productCatalogService;

            _categoryService = categoryService;
            _productService = productService;
            _superCategoryService = superCategoryService;
        }

        public ActionResult Index()
        {
            HomePageView homePageView = new HomePageView();
            homePageView.Categories = base.GetCategories();
            homePageView.SuperCategories = base.GetSuperCategories();
            homePageView.BasketSummary = base.GetBasketSummaryView();

            IEnumerable<ProductSummaryView> products = _productService.GetFeaturedProducts().Products;

            homePageView.Products = products;
            homePageView.FirstSuperCategoryId = _superCategoryService.FindAll().First().Id;
            return View(homePageView);
        }
    }
}
