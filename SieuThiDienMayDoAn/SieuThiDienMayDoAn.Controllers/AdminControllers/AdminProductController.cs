﻿using SieuThiDienMayDoAn.Controllers.ViewModels.Admin.ProductViewModel;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SieuThiDienMayDoAn.Controllers.AdminControllers
{
    public class AdminProductController:Controller
    {
        IProductService _productService;
        IBrandService _brandService;
        ICategoryService _categoryService;
        ISupplierService _supplierService;
        public AdminProductController(IProductService productService, IBrandService brandService, ICategoryService categoryService,
            ISupplierService supplierService)
        {
            _productService = productService;
            _brandService = brandService;
            _categoryService = categoryService;
            _supplierService = supplierService;

        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddProductPage()
        {
            AddProductPageView addproductPageView = new AddProductPageView();

            addproductPageView.Brands = _brandService.FindAll();
            addproductPageView.Categories = _categoryService.FindAll();
            addproductPageView.Suppliers = _supplierService.FindAll();
            return View(addproductPageView);
        }

        public ActionResult UpdateProductPage()
        {
            return View();
        }
    }
}
