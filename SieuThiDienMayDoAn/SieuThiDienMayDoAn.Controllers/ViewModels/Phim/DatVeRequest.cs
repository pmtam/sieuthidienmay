﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.Phim
{
    public class DatVeRequest
    {
        public int PhimId { get; set; }
        public int RapId { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Sdt { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public int SoLuongVe { get; set; }
        public string Ghe { get; set; }
    }
}