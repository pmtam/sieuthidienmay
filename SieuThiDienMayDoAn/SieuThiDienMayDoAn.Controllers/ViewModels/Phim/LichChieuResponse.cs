﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.Phim
{
    public class LichChieuResponse
    {
        public string NgayChieu { get; set; }
        public List<string> Times { get; set; }
    }
}