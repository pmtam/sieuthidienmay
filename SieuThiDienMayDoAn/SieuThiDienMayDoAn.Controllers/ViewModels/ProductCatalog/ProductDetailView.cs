﻿using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog
{
    public class ProductDetailView : BaseProductCatalogPageView
    {
        public ProductView Product { get; set; }
        public int CategoryId { get; set; }
        public string BrandName { get; set; }
    }
}
