﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog
{
    public class SearchProductsView: BaseProductCatalogPageView
    {
        public string SearchStr { get; set; }
    }
}
