﻿using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.SuperCategories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class SuperCategoryRepository: Repository<SuperCategory, int>, ISuperCategoryRepository
    {
        public SuperCategoryRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
