﻿
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Ves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class VeRepository:Repository<Ve,int>, IVeRepository
    {
         public VeRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
