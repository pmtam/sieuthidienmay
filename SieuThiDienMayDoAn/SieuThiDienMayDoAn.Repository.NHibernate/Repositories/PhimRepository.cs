﻿
using NHibernate;
using NHibernate.Criterion;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Phims;
using SieuThiDienMayDoAn.Repository.NHibernate.Repositories;
using SieuThiDienMayDoAn.Repository.NHibernate.SessionStorage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class PhimRepository : Repository<Phim, int>, IPhimRepository
    {
         public PhimRepository(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IEnumerable<Phim> searchPhim(string searchStr)
        {
            ICriteria criteriaQuery =
            SessionFactory.GetCurrentSession().CreateCriteria(typeof(Phim));
            string[] strs = searchStr.Split(new char[] { ' ' });
            var conjunction = Restrictions.Conjunction();
            var disjunction = Restrictions.Disjunction();
            for (int i = 0; i < strs.Count(); i++)
            {
                disjunction.Add(Restrictions.Like("TenPhim", strs[i], MatchMode.Anywhere));
                disjunction.Add(Restrictions.Like("NuocSanXuat", strs[i], MatchMode.Anywhere));
                disjunction.Add(Restrictions.Like("Hang", strs[i], MatchMode.Anywhere));
                disjunction.Add(Restrictions.Like("MoTa", strs[i], MatchMode.Anywhere));
                conjunction.Add(disjunction);
                disjunction = new Disjunction();
            }
            
            criteriaQuery.Add(conjunction);

            return criteriaQuery.List<Phim>();
        }

        public  string UnicodeToAscii(string unicodeString)
        {

            // Create two different encodings.
            Encoding ascii = Encoding.ASCII;
            Encoding unicode = Encoding.Unicode;

            // Convert the string into a byte array. 
            byte[] unicodeBytes = unicode.GetBytes(unicodeString);

            // Perform the conversion from one encoding to the other. 
            byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

            // Convert the new byte[] into a char[] and then into a string. 
            char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
            ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
            string asciiString = new string(asciiChars);

            return asciiString;


        }
    }
}
