﻿
using NHibernate;
using NHibernate.Criterion;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Raps;
using SieuThiDienMayDoAn.Repository.NHibernate.SessionStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class RapRepository:Repository<Rap,int>, IRapRepository
    {

         public RapRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
        public List<Rap> GetRapsByPhim(int phimId)
        {
            ICriteria criteriaQuery =
                       SessionFactory.GetCurrentSession().CreateCriteria(typeof(Rap)).CreateAlias("PhimChieu","phimChieu");
            criteriaQuery.Add(Restrictions.Eq("phimChieu.Id", phimId));
            
            return (List<Rap>)criteriaQuery.List<Rap>();
        }

        public override void AppendCriteria(ICriteria criteria)
        {
            criteria.CreateAlias("PhimChieu", "phimChieu");

        }

        public Rap GetRapsByPhimAndRap(int phimId,int rapId)
        {
            ICriteria criteriaQuery =
                       SessionFactory.GetCurrentSession().CreateCriteria(typeof(Rap)).CreateAlias("PhimChieu", "phimChieu");
            criteriaQuery.Add(Restrictions.Eq("phimChieu.Id", phimId));
            criteriaQuery.Add(Restrictions.Eq("Id", rapId));
            //criteriaQuery.Add
            return (Rap)criteriaQuery.UniqueResult();
        }

    }
}
