﻿using NHibernate;
using NHibernate.Criterion;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Model.SuperCategories;
using SieuThiDienMayDoAn.Repository.NHibernate.SessionStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class ProductRepository : Repository<Product, int>, IProductRepository
    {
        public ProductRepository(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IEnumerable<Product> SearchProducts(string searchStr, int index, int numberOfRecordsPerPage)
        {
            ICriteria criteriaQuery =
            SessionFactory.GetCurrentSession().CreateCriteria(typeof(Product));
            string[] strs = searchStr.Split(new char[] { ' ' });
            var conjunction = Restrictions.Conjunction();
            var disjunction = Restrictions.Disjunction();
            for (int i = 0; i < strs.Count(); i++)
            {
                disjunction.Add(Restrictions.Like("Name", strs[i], MatchMode.Anywhere));
                disjunction.Add(Restrictions.Like("Description", strs[i], MatchMode.Anywhere));
                conjunction.Add(disjunction);
                disjunction = new Disjunction();
            }
            criteriaQuery.Add(conjunction);

            criteriaQuery.SetFirstResult(index * numberOfRecordsPerPage);
            criteriaQuery.SetMaxResults(numberOfRecordsPerPage);

            return criteriaQuery.List<Product>();
        }


        public IEnumerable<decimal> GetPricesOfProductsSearchResult(string searchStr, int index, int numberOfRecordsPerPage)
        {
            IEnumerable<Product> products = SearchProducts(searchStr, index, numberOfRecordsPerPage);
            List<decimal> prices = new List<decimal>();
            foreach (var p in products)
            {
                prices.Add(p.Price);
            }
            var pricesGroupBy = prices.GroupBy(price => price);
            List<decimal> result = new List<decimal>();
            foreach (var pg in pricesGroupBy)
            {
                result.Add(pg.First());
            }

            return result;
            
        }


        public IEnumerable<Product> GetProductByCategory(int categoryId)
        {
            ICriteria criteriaQuery =
                       SessionFactory.GetCurrentSession().CreateCriteria(typeof(Product)).CreateAlias("ProductCategory", "Category");

            criteriaQuery.Add(Restrictions.Eq("Category.Id",categoryId));


            return criteriaQuery.List<Product>();
        }

        public override void AppendCriteria(ICriteria criteria)
        {
            criteria.CreateAlias("ProductCategory", "Category");


        }

    }
}
