﻿using NHibernate;
using NHibernate.Criterion;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Review;
using SieuThiDienMayDoAn.Repository.NHibernate.SessionStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class ReviewRepository :Repository<Review, int>, IReviewRepository
    {
        public ReviewRepository(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IEnumerable<Review> GetReviewsByProduct(int productId)
        {
            ICriteria criteriaQuery =
                       SessionFactory.GetCurrentSession().CreateCriteria(typeof(Review)).CreateAlias("Product", "product");

            criteriaQuery.Add(Restrictions.Eq("product.Id", productId));


            return criteriaQuery.List<Review>();
        }

        public IEnumerable<Review> GetReviewsByProduct(int productId,int index,int count)
        {
            ICriteria criteriaQuery =
                       SessionFactory.GetCurrentSession().CreateCriteria(typeof(Review)).CreateAlias("Product", "product");

            criteriaQuery.Add(Restrictions.Eq("product.Id", productId));

            criteriaQuery.SetFirstResult(index * count);
            criteriaQuery.SetMaxResults(count);
            return criteriaQuery.List<Review>();
        }

        public override void AppendCriteria(ICriteria criteria)
        {
            criteria.CreateAlias("Product", "product");


        }
    }
}
