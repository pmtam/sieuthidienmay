﻿using NHibernate;
using SieuThiDienMayDoAn.Repository.NHibernate.SessionStorage;
using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Infrastructure.Querying;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Model.Products;
using NHibernate.Criterion;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public abstract class Repository<T, TEntityKey> where T : IAggregateRoot
    {
        private IUnitOfWork _uow;
        public Repository(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public TEntityKey Add(T entity)
        {
            Object ojb = SessionFactory.GetCurrentSession().Save(entity);
            return (TEntityKey)ojb;
        }

        public void Remove(T entity)
        {
            SessionFactory.GetCurrentSession().Delete(entity);
        }

        public void Save(T entity)
        {
            SessionFactory.GetCurrentSession().SaveOrUpdate(entity);
        }

        public T FindBy(TEntityKey id)
        {
            return SessionFactory.GetCurrentSession().Get<T>(id);
        }

        public IEnumerable<T> FindAll()
        {
            ICriteria criteriaQuery =
            SessionFactory.GetCurrentSession().CreateCriteria(typeof(T));
            return (List<T>)criteriaQuery.List<T>();
        }

        public IEnumerable<T> FindAll(int index, int count)
        {
            ICriteria criteriaQuery =
            SessionFactory.GetCurrentSession().CreateCriteria(typeof(T));
            criteriaQuery.SetFirstResult(index * count);
            criteriaQuery.SetMaxResults(count);
            return (List<T>)criteriaQuery.List<T>();
        }

        public IEnumerable<T> FindBy(Query query)
        {
            ICriteria criteriaQuery =
            SessionFactory.GetCurrentSession().CreateCriteria(typeof(T));
            AppendCriteria(criteriaQuery);
            query.TranslateIntoNHQuery<T>(criteriaQuery);

            query.TranslateIntoNHQuery<T>(criteriaQuery);




            return criteriaQuery.List<T>();
        }


        public IEnumerable<T> FindBy(Query query, int index, int count)
        {
            ICriteria criteriaQuery =
            SessionFactory.GetCurrentSession().CreateCriteria(typeof(T));
            AppendCriteria(criteriaQuery);
            query.TranslateIntoNHQuery<T>(criteriaQuery);
            criteriaQuery.SetFirstResult(index * count);
            criteriaQuery.SetMaxResults(count);
            return criteriaQuery.List<T>();
        }
        public virtual void AppendCriteria(ICriteria criteria)
        {
        }
    }
}
