﻿
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.DatVes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class DatVeRepository:Repository<DatVe,int>,  IDatVeRepository
    {
         public DatVeRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
