﻿
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.PhimUsers;
using SieuThiDienMayDoAn.Repository.NHibernate.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.Repositories
{
    public class PhimUserRepository:Repository<PhimUser,int>, IUserRepository
    {
        public PhimUserRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
