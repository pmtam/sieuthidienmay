﻿
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.ChiTietDatVes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class ChiTietDatVeRepository:Repository<ChiTietDatVe,int> , IChiTietDatVeRepository
    {
         public ChiTietDatVeRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
