﻿using NHibernate;
using NHibernate.Criterion;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Repository.NHibernate.SessionStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class DeliveryAddressRepository: Repository<DeliveryAddress, int>, IDeliveryAddressRepository
    {
        public DeliveryAddressRepository(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IEnumerable<DeliveryAddress> GetDeliveryAddressesByCustomer(int customerId)
        {
            ICriteria criteria = SessionFactory.GetCurrentSession()
            .CreateCriteria(typeof(DeliveryAddress)).CreateAlias("Customer", "customer").
            Add(Restrictions.Eq("customer.Id", customerId));

            return criteria.List<DeliveryAddress>();
        }

    }
}
