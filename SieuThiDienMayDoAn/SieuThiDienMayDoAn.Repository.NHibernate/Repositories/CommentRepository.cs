﻿using NHibernate;
using NHibernate.Criterion;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Review;
using SieuThiDienMayDoAn.Repository.NHibernate.SessionStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class CommentRepository: Repository<Comment, int>, ICommentRepository
    {
        public CommentRepository(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IEnumerable<Comment> GetCommentsByReview(int reviewId)
        {
            ICriteria criteriaQuery =
                       SessionFactory.GetCurrentSession().CreateCriteria(typeof(Comment)).CreateAlias("Review", "review");

            criteriaQuery.Add(Restrictions.Eq("review.Id", reviewId));


            return criteriaQuery.List<Comment>();
        }

        public IEnumerable<Comment> GetCommentsByReview(int reviewId,int index, int count)
        {
            ICriteria criteriaQuery =
                       SessionFactory.GetCurrentSession().CreateCriteria(typeof(Comment)).CreateAlias("Review", "review");

            criteriaQuery.Add(Restrictions.Eq("review.Id", reviewId));
            criteriaQuery.SetFirstResult(index * count);
            criteriaQuery.SetMaxResults(count);

            return criteriaQuery.List<Comment>();
        }
    }
}
