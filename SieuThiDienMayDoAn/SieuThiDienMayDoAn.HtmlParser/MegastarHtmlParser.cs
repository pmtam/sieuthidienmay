﻿
using HtmlAgilityPack;
using SieuThiDienMayDoAn.Model.Phims;
using SieuThiDienMayDoAn.Model.Raps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace SieuThiDienMayDoAn.HtmlParser
{

    public class MegastarHtmlParser
    {
        IPhimService _phimService;
        IRapService _rapService;


        public MegastarHtmlParser(IPhimService phimService, IRapService rapService)
        {
            _phimService = phimService;
            _rapService = rapService;
           

        }

        public void GetDanhSachPhim(string url,bool dangChieu)
        {
            HtmlWeb hw = new HtmlWeb();
            
            HtmlDocument doc = hw.Load(url);


            List<string> originalMovieNames = GetAllOrginalMovieNames();


            List<Phim> movies = new List<Phim>();
            var nodes = doc.DocumentNode.SelectNodes("//div[@class='landingbody_item']");
            //ViewBag.Count = nodes.Count;
            string str = "Count:";
            StringBuilder sb = new StringBuilder();
            foreach (HtmlNode n in nodes)
            {

                string name = n.Elements("div").ElementAt(0).Elements("a").ElementAt(0).InnerText;

                string originalName = n.Elements("div").ElementAt(0).Elements("a").ElementAt(0).Attributes.AttributesWithName("href").ElementAt(0).Value.Split('/')[2];
                if (OriginalMovieNameIsExist(originalName, originalMovieNames))
                {
                    continue;
 
                }
                
                string newOriginalName1 = "";
                string newOriginalName2 = "";


                if (originalName.Contains("vn-dubbed"))
                {
                    string[] splitOriginalNames = originalName.Split(new string[] { "-vn-dubbed" }, StringSplitOptions.None);

                    newOriginalName1 = splitOriginalNames[0].Replace('-', ' ') + " (VN Dubbed)";
                    newOriginalName2 = splitOriginalNames[0].Replace('-', ' ') + " ( VN Dubbed)";
                    newOriginalName1 = newOriginalName1.Trim();
                    newOriginalName2 = newOriginalName2.Trim();

                }
                else if (originalName.Contains("megastar-picks"))
                {
                    string[] splitOriginalNames = originalName.Split(new string[] { "-megastar-picks" }, StringSplitOptions.None);

                    newOriginalName1 = splitOriginalNames[0].Replace('-', ' ') + " (MegaStar Picks)";
                    newOriginalName2 = splitOriginalNames[0].Replace('-', ' ') + " ( MegaStar Picks)";
                    newOriginalName1 = newOriginalName1.Trim();
                    newOriginalName2 = newOriginalName2.Trim();
                }
                else if (originalName.Contains("atmos"))
                {
                    string[] splitOriginalNames = originalName.Split(new string[] { "atmos" }, StringSplitOptions.None);

                    newOriginalName1 = splitOriginalNames[0].Replace('-', ' ') + " (ATMOS)";

                    newOriginalName2 = splitOriginalNames[0].Replace('-', ' ') + " ( ATMOS)";
                    if (splitOriginalNames.Count() >= 2)
                    {
                        newOriginalName1 += splitOriginalNames[1].Replace('-', ' ');
                        newOriginalName2 += splitOriginalNames[1].Replace('-', ' ');

                    }

                    newOriginalName1 = newOriginalName1.Trim();
                    newOriginalName2 = newOriginalName2.Trim();
                }
                else
                {
                    newOriginalName1 = originalName.Replace('-', ' ').Trim();
                    newOriginalName2 = originalName.Replace('-', ' ').Trim();

                }

                string description = n.Elements("div").ElementAt(0).InnerText;

                //str += aNode.InnerHtml + ",";
                string[] strs = n.Elements("a").ElementAt(0).InnerHtml.Split(new char[] { '"' });

                string urlImage = "http://www.megastar.vn" + strs[1];

                Phim m = new Phim()
                {
                    TenPhim = name,
                    MoTa = description,
                    Image = urlImage,
                    DangChieu = dangChieu,
                    OriginalName = originalName
                };

                movies.Add(m);

                int phimId = _phimService.Add(m);

                List<Rap> cinemas = GetCinemas(newOriginalName1, phimId);
                if (cinemas.Count <= 0)
                {
                    cinemas = GetCinemas(newOriginalName2, phimId);
                }

                foreach (Rap r in cinemas)
                {
                    int rapId = _rapService.Add(r);

                }

            }

        }

        public List<Rap> GetCinemas(string originalMovieName, int phimId)
        {
            List<Rap> cinemas = new List<Rap>();
            int increment = 0;
            String URLString = "http://www.megastar.vn/megastarXMLData.aspx?RequestType=GetCinemaList&&MovieName=" + originalMovieName + "&&visLang=1";

            var document = new XPathDocument(URLString);
            XPathNavigator navigator = document.CreateNavigator();

            XPathNodeIterator nodeArray = navigator.Select("//Cinema_strID");

            string xmlStr = "";

            Phim phim = _phimService.FindBy(phimId);


            while (nodeArray.MoveNext())
            {
                XPathNavigator currentNavigator = nodeArray.Current;
                string id = currentNavigator.Value;

                Rap r = new Rap() { MaRap = id, PhimChieu = phim };


               
                //r.LichChieu = ngayChieuStr.ToString();
                r.LichChieu = GetDateTimes(id,originalMovieName);
                cinemas.Add(r);


            }

            XPathNodeIterator nameArray = navigator.Select("//Cinema_strName");

            while (nameArray.MoveNext())
            {
                XPathNavigator currentNavigator = nameArray.Current;

                string name = currentNavigator.Value;
                xmlStr += name;
                if (cinemas.Count> increment)
                {
                    cinemas[increment++].TenRap = name;
                }

            }

            increment = 0;
            XPathNodeIterator addressArray = navigator.Select("//Cinema_strAddress");

            while (addressArray.MoveNext())
            {
                XPathNavigator currentNavigator = addressArray.Current;

                string address = currentNavigator.Value;
                xmlStr += address;
                if (cinemas.Count > increment)
                {
                    cinemas[increment++].DiaChi = address;
                }
            }

            increment = 0;
            XPathNodeIterator phoneArray = navigator.Select("//Cinema_strPhone");

            while (phoneArray.MoveNext())
            {
                XPathNavigator currentNavigator = phoneArray.Current;

                string phone = currentNavigator.Value;
                xmlStr += phone;
                if (cinemas.Count > increment)
                {
                    cinemas[increment++].DienThoai = phone;
                }
            }

            return cinemas;
        }


        public string GetDateTimes(string maRap, string originalMovieName)
        {
            String ncURLString = "http://www.megastar.vn/megastarXMLData.aspx?RequestType=GetSessionTimes&&CinemaID="+maRap.Trim()+"&&MovieName="+originalMovieName.Trim()+"&&Time=TodayAndTomorrow&&visLang=1";

            var ncdocument = new XPathDocument(ncURLString);
            XPathNavigator ncnavigator = ncdocument.CreateNavigator();

            XPathNodeIterator ncnodeArray = ncnavigator.Select(".//date");

            //List<NgayChieu> ngayChieus = new List<NgayChieu>();
            StringBuilder stringBuilder = new StringBuilder();

            while (ncnodeArray.MoveNext())
            {

                XPathNavigator nccurrentNavigator = ncnodeArray.Current;
                string name = nccurrentNavigator.GetAttribute("name", "");

                stringBuilder.Append(name);


                string times = "";
                int increment = 0;
                foreach (XPathNavigator innerNav in nccurrentNavigator.Select(".//value"))
                {
                    string value = innerNav.Value;
                    if (increment == 0)
                    {
                        times += "//";
                    }
                    times += value.Split(new char[] { '>', '<' })[2] + ",";

                    increment++;


                }
                stringBuilder.Append(times);

            }

            return stringBuilder.ToString();
        }


        public List<string> GetAllOrginalMovieNames()
        {
            List<string> originalMovieNames = new List<string>();
            IEnumerable<Phim> phims = _phimService.FindAll();
            foreach (Phim p in phims)
            {
                originalMovieNames.Add(p.OriginalName);
            }
            return originalMovieNames;
        }

        public bool OriginalMovieNameIsExist(string originalMovieName, List<String> originalMovieNames)
        {
            return originalMovieNames.Exists(str => str.Equals(originalMovieName));
        }

       

    }
}
