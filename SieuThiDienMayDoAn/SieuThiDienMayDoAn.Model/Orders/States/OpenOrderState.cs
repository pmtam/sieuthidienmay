﻿using SieuThiDienMayDoAn.Infrastructure.Events;
using SieuThiDienMayDoAn.Model.Orders.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Orders.States
{
    public class OpenOrderState : OrderState
    {
        public override OrderStatus Status
        {
            get { return OrderStatus.Open; }
        }
        public override bool CanAddProduct()
        {
            return true;
        }

        public override void Submit(Order order)
        {
            
                order.SetStateTo(OrderStates.Submitted);
            DomainEvents.Raise(new OrderSubmittedEvent() { Order = order });
        }
    }
}
