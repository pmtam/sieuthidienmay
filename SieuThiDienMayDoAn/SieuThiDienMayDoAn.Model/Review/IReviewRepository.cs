﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Review
{
    public interface IReviewRepository:IRepository<Review,int>
    {
        IEnumerable<Review> GetReviewsByProduct(int productId);
        IEnumerable<Review> GetReviewsByProduct(int productId, int index, int count);
    }
}
