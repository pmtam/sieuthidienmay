﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Review
{
    public class Review : EntityBase<int>, IAggregateRoot
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
        public string UserName { get; set; }
        public Product Product { get; set; }
        public IList<Comment> Comments { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
