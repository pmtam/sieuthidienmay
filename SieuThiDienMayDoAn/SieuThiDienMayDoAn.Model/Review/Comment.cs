﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Review
{
    public class Comment : EntityBase<int>, IAggregateRoot
    {
        public string Content{get;set;}
        public Review Review { get; set; }
        public string UserName{get;set;}

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
