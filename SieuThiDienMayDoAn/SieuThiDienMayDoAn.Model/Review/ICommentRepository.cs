﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Review
{
    public interface ICommentRepository : IRepository<Comment, int>
    {
        IEnumerable<Comment> GetCommentsByReview(int reviewId);
        IEnumerable<Comment> GetCommentsByReview(int reviewId, int index, int count);
    }
}
