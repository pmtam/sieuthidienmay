﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Categories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.SuperCategories
{
    public class SuperCategory : EntityBase<int>, IAggregateRoot
    {
        public string Name { get; set; }
        public IList<Category> Categories { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
