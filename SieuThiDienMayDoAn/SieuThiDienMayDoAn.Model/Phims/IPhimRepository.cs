﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Phims
{
    public interface IPhimRepository:IRepository<Phim,int>
    {
        IEnumerable<Phim>  searchPhim(string searchStr);
    }
}
