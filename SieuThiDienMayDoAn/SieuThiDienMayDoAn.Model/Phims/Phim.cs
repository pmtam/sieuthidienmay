﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Phims
{
    public class Phim:EntityBase<int>,IAggregateRoot
    {
        public int Id { get; set; }
        public string TenPhim { get; set; }
        public string KhoiChieu { get; set; }
        public string TheLoai { get; set; }
        public string DienVien { get; set; }
        public string DaoDien { get; set; }
        public string DoDai { get; set; }
        public string PhienBan { get; set; }
        public string Hang { get; set; }
        public string NuocSanXuat { get; set; }
        public string MoTa { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public bool DangChieu { get; set; }
        public string OriginalName { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
        
    }
}
