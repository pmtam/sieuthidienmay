﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Brands;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Model.SuperCategories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Categories
{
    public class Category : EntityBase<int>, IAggregateRoot, IProductAttribute
    {
        public string Name { get; set; }
        public SuperCategory SuperCategoryInfo { get; set; }
        public IList<Product> Products { get; set; }


        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
