﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.PhimUsers
{
    public class PhimUser:EntityBase<int>,IAggregateRoot
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }

        public string Email { get; set; }
        public string Sdt { get; set; }
        public string Image { get; set; }
        public string LastName { get; set; }
        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
