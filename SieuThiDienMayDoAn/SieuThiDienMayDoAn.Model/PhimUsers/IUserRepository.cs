﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.PhimUsers
{
    public interface IUserRepository : IRepository<PhimUser, int>
    {
    }
}
