﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Shipping
{
    public class NullDeliveryOption : IDeliveryOption
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal FreeDeliveryThreshold
        {
            get { return 0; }
        }
        public decimal Cost
        {
            get { return 0; }
        }

        public decimal GetDeliveryChargeForBasketTotalOf(decimal total)
        {
            return 0;
        }
    }
}
