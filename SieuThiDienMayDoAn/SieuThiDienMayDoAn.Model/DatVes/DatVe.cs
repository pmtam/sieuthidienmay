﻿
using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.PhimUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.DatVes
{
    public class DatVe:EntityBase<int>,IAggregateRoot
    {
        public PhimUser UserDatVe { get; set; }
        public DateTime NgayDatVe { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
