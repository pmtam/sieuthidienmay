﻿
using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.DatVes;
using SieuThiDienMayDoAn.Model.Ves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.ChiTietDatVes
{
    public class ChiTietDatVe:EntityBase<int>,IAggregateRoot
    {
        public DatVe DatVeInfo { get; set; }

        public int SoLuong { get; set; }
        public decimal TongGia { get; set; }
        public Ve VeInfo { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
