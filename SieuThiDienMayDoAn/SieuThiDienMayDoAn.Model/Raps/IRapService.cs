﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Raps
{
    public interface IRapService:IService<Rap,int>
    {
        List<Rap> GetRapsByPhim(int phimId);
        Rap GetRapsByPhimAndRap(int phimId, int rapId);
    }
}
