﻿
using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Phims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Raps
{
    public class Rap:EntityBase<int>,IAggregateRoot
    {
        public int Id { get; set; }
        public string TenRap { get; set; }
        public string DiaChi { get; set; }
        public string Image { get; set; }
        public string MaRap { get; set; }
        public string DienThoai { get; set; }
        public string LichChieu { get; set; }
        
        public Phim PhimChieu { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
