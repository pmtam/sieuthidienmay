﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Ves
{
    public class Ve: EntityBase<int>,IAggregateRoot
    {
        public string Loai { get; set; }
        public string Gia { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
